def exercise_one(): ## FIRST PART EXERCISE ONE
    for num in range(1, 101):
        output = ""

        if num % 3 == 0:
            output += "Three"

        if num % 5 == 0:
            output += "Five"

        # If neither condition is met, print the number itself
        if not output:
            output = str(num)

        print(output)
    pass

def is_colorful(number): ## FIRST PART EXERCISE TWO 
    digits = [int(digit) for digit in str(number)]# Converting strings values to digit
    products = set() # storing unique vvalues

    # Iterate through all consecutive subsets of digits
    for i in range(len(digits)):
        product = 1
        for j in range(i, len(digits)):
            product *= digits[j]
            if product in products:
                return False
            products.add(product)
    return True
number1 = 263
number2 = 236
number3 = 2532

print(f"{number1} ---> {is_colorful(number1)}")
print(f"{number2} ---> {is_colorful(number2)}")
print(f"{number3} ---> {is_colorful(number3)}")   

def calculate(lst):## FIRST PART EXERCISE THREE
    if not isinstance(lst, list): # if the item is not a list, then we will return False directly.
        return False

    total = 0
    for item in lst:
        if isinstance(item, str) and item.lstrip('-').isdigit():
            total += int(item)

    return total


print(calculate(['4', '3', '-2']))  
print(calculate(453))  
print(calculate(['nothing', 3, '8', 2, '1'])) 
print(calculate('54'))  


def are_anagrams(word1, word2): ## FIRST PART EXERCISE FOUR
    return sorted(word1) == sorted(word2) # Checking if the words are anagrams(Comparision between two words)

def anagrams(word, word_list):
    return [w for w in word_list if are_anagrams(word, w)] # extracting words from the list and checking the same with
                                                        # With the are_anagrams functions. If it matches with word
                                                        # in the word list, then fetching those words from the word list.

# Examples
print(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']))  
print(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']))  
print(anagrams('laser', ['lazing', 'lazy', 'lacer']))  
