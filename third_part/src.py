import requests
import gzip
import json
import csv
import selenium

def http_request():
    return requests.post(url="https://httpbin.org/anything",data={"isadmin": 1}).text

def exercise_two():
    filee = open('Products.csv','w',newline='')
    writer = csv.writer(filee)
    writer.writerow(["Name","Price"])

    #Opening the file and converting it to json
    with gzip.open('data/data.json.gz', 'rb') as filee:
        data = json.loads(f.read())

    for products in data["Bundles"]:
        index = 0
        if "Products" in products and "Name" in products:
            product_name = products["Name"][0:30]
            if "Promotion" in products["Products"][0]:
                
                index = 1
            if products["Products"][index]["Price"] != None:
                price = round(products["Products"][index]["Price"],1)
            else:
                price = "null"
            isinStock = products["Products"][index]["IsInStock"]
            if isinStock == False:
                product_id = products["Products"][index]["Barcode"] # Getting the product ID
                print("Product ID : " + product_id + " - Product Name : " + product_name)
            if price !="null":
                print("You can buy " + product_name + " from our store at " + str(price) + "€")
                writer.writerow([product_name,str(price)])
            else:
                print("No price Found for " + product_name)
        else:
            print("Unknown error")



