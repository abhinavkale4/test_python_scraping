import random
def random_gen():
    for _ in range(15):
        num = random.randint(10, 20)
        print(num)
        if num == 15:
            break
    pass


def decorator_to_str(func):
    # todo exercise 2
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return str(result)
    return wrapper
    
    return func


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']


def ignore_exception(exception):
    # todo exercise 3
    
    return lambda x: x


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap
import unittest

class TestCacheDecorator(unittest.TestCase):
    def setUp(self):
        self.decorated_function_calls = 0

    def test_cache_decorator_caches_result(self):
        # Arrange
        @CacheDecorator()
        def sample_function(x):
            self.decorated_function_calls += 1
            return x + 1

        # Act
        result1 = sample_function(5)
        result2 = sample_function(5)

        # Assert
        self.assertEqual(result1, result2)
        self.assertEqual(self.decorated_function_calls, 1)

    def test_cache_decorator_handles_multiple_arguments(self):
        # Arrange
        @CacheDecorator()
        def sample_function(x, y):
            self.decorated_function_calls += 1
            return x + y

        # Act
        result1 = sample_function(3, 4)
        result2 = sample_function(3, 4)

        # Assert
        self.assertEqual(result1, result2)
        self.assertEqual(self.decorated_function_calls, 1)

    def test_cache_decorator_does_not_cache_different_arguments(self):
        # Arrange
        @CacheDecorator()
        def sample_function(x):
            self.decorated_function_calls += 1
            return x + 1

        # Act
        result1 = sample_function(5)
        result2 = sample_function(10)

        # Assert
        self.assertNotEqual(result1, result2)
        self.assertEqual(self.decorated_function_calls, 2)

    def test_cache_decorator_handles_keyword_arguments(self):
        # Arrange
        @CacheDecorator()
        def sample_function(x, y):
            self.decorated_function_calls += 1
            return x + y

        # Act
        result1 = sample_function(x=3, y=4)
        result2 = sample_function(x=3, y=4)

        # Assert
        self.assertEqual(result1, result2)
        self.assertEqual(self.decorated_function_calls, 1)

if __name__ == '__main__':
    unittest.main()



class MetaInherList(type):
    # todo exercise 5
    pass


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

